//Load the libraries
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");

//Create an instance of the application
var app = express();

//define the path
app.use("/libs", express.static(path.join(__dirname,"bower_components")));//double check html file is pointing to this
app.use(express.static(path.join(__dirname,"public")));//double check html file
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var allRegistration = [];

// var registrationDetails = function(registrationDetails){
//     return({
//         name: registrationDetails.name,
//         gender: registrationDetails.gender,
//         DoB: registrationDetails.dob,
//         addess: registrationDetails.address,
//         country: registrationDetails.country,
//         contactno: registrationDetails.contactno,
//         email: registrationDetails.email,
//         password: registrationDetails.password
//     });
// };

// to set up a function to receive get request
app.post("/register", function(req,res, err){
    console.log("post received");
    
    console.log(">>req: " + JSON.stringify(req.body) );
    allRegistration.push(req.body);

    // allRegistration.push(registrationDetails(req));
    console.log("All orders:\n %s", JSON.stringify(allRegistration));
 });


//define server port
//const NODE_PORT = process.env.NODE_PORT || 3000;
// Set the port
app.set("port",  parseInt(process.env.APP_PORT) || parseInt(process.argv[2]) || 3000)

// Start the app
app.listen(app.get("port"), function() {   
    console.log("App started at %s at port %d", new Date(), app.get("port"));
})