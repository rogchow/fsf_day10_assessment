(function(){
    angular.module("RegisterApp", ["RegisterApp"])
        .controller("RegisterCtrl", RegisterCtrl); 

    
   
    RegisterCtrl.$inject= ['$window','$http'];
    function RegisterCtrl($window, $http){
        var vm = this;  

       
        
        vm.registrationDetails = {
            name: '',
            gender: '',
            dob: '',
            address: '',
            country: '',
            contactno: '',
            email: '',
            password: ''
        }
       

        // call functions ONLY inside controller
        // register(vm.registrationDetails);
        vm.register = function(){
            console.log(">>register initiated");
            console.log("req obj:" + JSON.stringify(vm.registrationDetails));
            $http.post("/register", vm.registrationDetails)
            .then(function(result){
            $window.alert("registration successful: " + result);    
            }).catch(function(err){
                $window.alert("registration failed: " + err);
            });
        }

        
    };

    console.log("executed registrationDetails!");


    function initForm(registerCtrl)   
    {
        registration.name = "";
        registration.gender ="";
        registration.dob = "";
        registration.address ="";
        registration.country = "";
        registration.contactno ="";
        registration.email = "";
        registrationDetails.password = "";     
        console.log("init is called!");
    };

    console.log("program end");


})();